import express from "express";
import authRouter from "./src/routes/auth.router.js";

const app = express();

app.use(express.json());

app.use("/auth", authRouter);

app.listen(3000, ()=>{
    console.log("server started at 3000");
})

app.get("/", function (req, res){
    res.send("<p>API desarrollada por Alexis Sanhueza</p>")
})
